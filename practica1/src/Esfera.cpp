// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <Glut/glut.h>
#elif defined(_WIN32) || defined(_WIN64)
#include <GLUT/glcmolorut.h>
#else
#include <GL/glut.h>
#endif
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=5;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
    glColor3ub(255,255,0);
    glEnable(GL_LIGHTING);
    glPushMatrix();
    glTranslatef(centro.x,centro.y,0);
    glutSolidSphere(radio,15,15);
    glPopMatrix();
    
}

void Esfera::Mueve(float t)
{
    centro=centro+velocidad*t;
    radio=radio-0.005;
    if (radio<0.1){
        radio=0.5;
    }
}
