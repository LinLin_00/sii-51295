// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDOCLIENTE_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDOCLIENTE_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <vector>
#include "Plano.h"
#include <sys/mman.h>
#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"

class CMundoCliente
{
public:
    void Init();
    CMundoCliente();
    virtual ~CMundoCliente();
    
    void InitGL();
    void OnKeyboardDown(unsigned char key, int x, int y);
    void OnTimer(int value);
    void OnDraw();
    
    Esfera esfera;
    std::vector<Plano> paredes;
    Plano fondo_izq;
    Plano fondo_dcho;
    Raqueta jugador1;
    Raqueta jugador2;
    
    int puntos1;
    int puntos2;
    
    int fd_fifoCoord;//Tuberia que recibe las coordenadas del servidor
    int fd_fifoTeclas;//Tuberia que envia las teclas pulsadas
    
    int fd_mem;
    DatosMemCompartida datosBot;
    DatosMemCompartida *pdatosBot;
    char *proyeccion;
};


#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
