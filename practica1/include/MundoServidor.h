// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <vector>
#include "Plano.h"
#include <sys/mman.h>
#include "Esfera.h"
#include "Raqueta.h"

class CMundoServidor
{
public:
    void Init();
    CMundoServidor();
    virtual ~CMundoServidor();
    
    void RecibeComandosJugador();
    void InitGL();
    void OnKeyboardDown(unsigned char key, int x, int y);
    void OnTimer(int value);
    void OnDraw();
    
    Esfera esfera;
    std::vector<Plano> paredes;
    Plano fondo_izq;
    Plano fondo_dcho;
    Raqueta jugador1;
    Raqueta jugador2;
    
    int puntos1;
    int puntos2;
    int fd_fifoLogger;
    
    int fd_fifoCoord;//Tuberia que ENVIA las coordenadas a cliente
    int fd_fifoTeclas;//Tuberia que RECIBE las teclas pulsadas del cliente
    
    pthread_t thid1;
    
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
