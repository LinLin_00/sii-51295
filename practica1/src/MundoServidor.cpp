// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#include "MundoServidor.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <fstream>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <Glut/glut.h>
#elif defined(_WIN32) || defined(_WIN64)
#include <GLUT/glcmolorut.h>
#else
#include <GL/glut.h>
#endif

#define MAX 200
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void* hilo_comandos(void* d)
{
    CMundoServidor* p=(CMundoServidor*)d;
    
    p->RecibeComandosJugador();
}

CMundoServidor::CMundoServidor()
{
    Init();
}

CMundoServidor::~CMundoServidor()
{
    
    printf("He entrado al destructor del MundoServidor\n");

    //Para fifo
    char mensajeroFin[MAX];
    sprintf(mensajeroFin,"-----Fin del juego-----\n");
    write(fd_fifoLogger,mensajeroFin,strlen(mensajeroFin)+1);
    close(fd_fifoLogger);
    
    write(fd_fifoCoord,mensajeroFin,strlen(mensajeroFin)+1);
    close(fd_fifoCoord);//Cerrar fifo que envia Coordenadas
    
    write(fd_fifoTeclas,mensajeroFin,strlen(mensajeroFin)+1);
    close(fd_fifoTeclas);//Cerrar fifo que recibe Teclas
    
}

void CMundoServidor::InitGL()
{
    //Habilitamos las luces, la renderizacion y el color de los materiales
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
    
    glMatrixMode(GL_PROJECTION);
    gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
    glDisable (GL_LIGHTING);
    
    glMatrixMode(GL_TEXTURE);
    glPushMatrix();
    glLoadIdentity();
    
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );
    
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);
    glColor3f(r,g,b);
    glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
    int len = strlen (mensaje );
    for (int i = 0; i < len; i++)
        glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
    
    glMatrixMode(GL_TEXTURE);
    glPopMatrix();
    
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    
    glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
    //Borrado de la pantalla
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    //Para definir el punto de vista
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    gluLookAt(0.0, 0, 17,  // posicion del ojo
              0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0)
              0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)
    
    ////////////////////////////
    //		AQUI EMPIEZA MI DIBUJO
    ////////////////////////////
    char cad[100];
    sprintf(cad,"Jugador1: %d\n",puntos1);
    print(cad,10,0,1,1,1);
    sprintf(cad,"Jugador2: %d\n",puntos2);
    print(cad,650,0,1,1,1);
    int i;
    for(i=0;i<paredes.size();i++)
        paredes[i].Dibuja();
    
    fondo_izq.Dibuja();
    fondo_dcho.Dibuja();
    jugador1.Dibuja();
    jugador2.Dibuja();
    esfera.Dibuja();
    
    
    ////////////////////////////
    //		AQUI TERMINA MI DIBUJO
    ////////////////////////////
    
    //Al final, cambiar el buffer
    glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{
    
    jugador1.Mueve(0.025f);
    jugador2.Mueve(0.025f);
    esfera.Mueve(0.025f);
    int i;
    for(i=0;i<paredes.size();i++)
    {
        paredes[i].Rebota(esfera);
        paredes[i].Rebota(jugador1);
        paredes[i].Rebota(jugador2);
    }
    
    jugador1.Rebota(esfera);
    jugador2.Rebota(esfera);
    
    if(fondo_izq.Rebota(esfera))
    {
        char mensajeroFifo2[MAX];
        esfera.centro.x=0;
        esfera.centro.y=rand()/(float)RAND_MAX;
        esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
        esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
        puntos2++;
        
        sprintf(mensajeroFifo2,"Jugador 2 ha obrenido 1 punto, ahora tiene : %d\n", puntos2);
        write(fd_fifoLogger,mensajeroFifo2,strlen(mensajeroFifo2)+1);
        
    }
    
    if(fondo_dcho.Rebota(esfera))
    {
        char mensajeroFifo1[MAX];
        esfera.centro.x=0;
        esfera.centro.y=rand()/(float)RAND_MAX;
        esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
        esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
        puntos1++;
        
        sprintf(mensajeroFifo1,"Jugador 1 ha obrenido 1 punto, ahora tiene : %d\n",puntos1);
        write(fd_fifoLogger,mensajeroFifo1,strlen(mensajeroFifo1)+1);
        
    }
    //Envio de coordenadas de servidor al cliente
    char cad[MAX];
    int aux_error;
    
    sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1, jugador1.y1, jugador1.x2, jugador1.y2, jugador2.x1, jugador2.y1, jugador2.x2, jugador2.y2, puntos1, puntos2);
//    puts(cad);
    if(aux_error<0)
        perror("Error de envio de coordenadas(Estoy en el MundoServidor)\n");
    
    aux_error=write(fd_fifoCoord,cad,strlen(cad)+1);
    if(aux_error<0){
        perror("Error de escritura en fifo coordenadas(Estoy en el mundo Servidor)\n");
        exit(1);
    }
    
  
    //Terminar si jugador 1 o 2 ha obtenido 3 puntos
    if(puntos1==3 ||puntos2==3)
        exit(0);
}
//Es el mundo el que controla teclas
void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
//    switch(key)
//    {
//        case 's':jugador1.velocidad.y=-4;break;
//        case 'w':jugador1.velocidad.y=4;break;
//        case 'l':jugador2.velocidad.y=-4;break;
//        case 'o':jugador2.velocidad.y=4;break;
//            
//    }
}
void CMundoServidor::RecibeComandosJugador()
{
    printf("He entrado al RecibirComandosJugador\n");
    int seguir =1;
    while (seguir==1) {
        usleep(10);
        char cad[100];
        int nLeido;
        nLeido=read(fd_fifoTeclas, cad, sizeof(cad));
        if(nLeido==-1){
            perror("Error al leer Teclas(Estoy en MundoServidor)\n");
            exit(1);
        }
        
        unsigned char key;
        sscanf(cad,"%c",&key);
        if(cad[0]=='-'){
            seguir =0;
        }
        else{
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
        }
        
    }
}
void CMundoServidor::Init()
{
    int aux_error;
    
    //Para inicializar esfera, ya que no calcula posicion
    esfera.centro.x=0;
    esfera.centro.y=0;
    
    Plano p;
    //pared inferior
    p.x1=-7;p.y1=-5;
    p.x2=7;p.y2=-5;
    paredes.push_back(p);
    
    //superior
    p.x1=-7;p.y1=5;
    p.x2=7;p.y2=5;
    paredes.push_back(p);
    
    fondo_izq.r=0;
    fondo_izq.x1=-7;fondo_izq.y1=-5;
    fondo_izq.x2=-7;fondo_izq.y2=5;
    
    fondo_dcho.r=0;
    fondo_dcho.x1=7;fondo_dcho.y1=-5;
    fondo_dcho.x2=7;fondo_dcho.y2=5;
    
    //a la izq
    jugador1.g=0;
    jugador1.x1=-6;jugador1.y1=-1;
    jugador1.x2=-6;jugador1.y2=1;
    
    //a la dcha
    jugador2.g=0;
    jugador2.x1=6;jugador2.y1=-1;
    jugador2.x2=6;jugador2.y2=1;
    
    //                     TUBERIA LOGGER               //
    //Apertura del que conecta con Logger que le envia la puntuacion
    fd_fifoLogger=open("/tmp/FifoPuntuacion",O_WRONLY);
    if(fd_fifoLogger==-1){
        perror("Error al abrir fifo que comunica con Logger (Estoy en MundoServidor)\n");
        exit(1);
    }
    else
        printf("He abierto el fifo de Logger para la puntuacion(Estoy en MundoServidor)\n");
    
    //                      TUBERIA COORDENADAS                //
    //Apertura del fifo para comunicacion entre servidor y cliente
    fd_fifoCoord=open("/tmp/FifoCoordenadas", O_WRONLY);//O_WRONLY porque hay que enviar informacion de coordenadas
    if(fd_fifoCoord==-1){
        perror("Error al abrir fifo que intenta enviar coordenadas a MundoCliente(Estoy en MundoServidor)\n");
        exit(1);
    }
    else
        printf("He abierto el fifo de coordenadas (Estoy en MundoServidor)\n");
    //                      TUBERIA TECLAS               //
    //Apertura del fifo para comunicacion entre servidor y cliente
    fd_fifoTeclas=open("/tmp/FifoTecla",O_RDONLY);
    printf("El valor de fd_fifoTecla es %d\n",fd_fifoTeclas);
    if(fd_fifoTeclas==-1){
        perror("Error al abrir fifo que intenta enviar teclas a MundoCliente(Estoy en MundoServidor)\n");
        exit(1);
    }
    else
        printf("He abierto el fifo de teclas (Estoy en MundoServidor)\n");
    
    //                      THREAD               //
    pthread_create(&thid1, NULL, hilo_comandos, this);
    printf("He creado el thread\n");
 
    
    
}
